import { PostController } from '../controllers';
import BaseRouter from './base.routes';

// клас PostRouter є нащадком класу BaseRouter
class PostRouter extends BaseRouter {
    constructor() {
        // виклик конструктора базового класу BaseRouter де ми передаємо екземпляр класу PostController
        super(new PostController());
    }
}

// отримання маршрутів
const { router } = new PostRouter();

// експорт маршрутів
export default router;
