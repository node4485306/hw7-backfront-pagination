import { BaseController } from './base.controller';
import { DatabaseService } from '../services';
import { Request, Response } from 'express';

// клас UserController є нащадком класу BaseController
export class UserController extends BaseController {
  constructor() {
    // виклик конструктора базового класу де вказано назву таблиці з якою буде працювати контролер
    super('users');
  }

  async createWithValidator(req: Request, res: Response) {
    try {
      // Створення нового запису у базі даних
      // req.body - дані запису для створення

      const data = await DatabaseService.create('users', req.body);

      // Успішна відповідь з кодом статусу 201 (створено) та об'єктом з повідомленням та створеним записом.
      res.status(201);
      res.json({ message: `users created`, data });
    } catch (error) {
      // Обробка помилки при створенні запису у базі даних.
      res.status(500);
      res.json({ message: `Error creating users` });
    }
  }
}
