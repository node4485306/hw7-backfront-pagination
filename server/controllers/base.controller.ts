import { Request, Response } from 'express';
import { DatabaseService } from '../services/database.service';

// Клас BaseController є базовим класом для всіх контролерів.
export class BaseController {
    private table: string;
    private databaseservice: DatabaseService;

    // Конструктор класу BaseController, приймає назву таблиці як аргумент та зберігає її в приватній властивості table.
    constructor(table: string) {
        this.table = table;
        this.databaseservice = new DatabaseService;

        // Прив'язка контексту для кожного методу класу, щоб забезпечити правильний контекст `this` при виклику цих методів.
        // Це необхідно, оскільки методи будуть передаватися як обробники подій у маршрутах Express.
        this.create = this.create.bind(this);
        this.getList = this.getList.bind(this);
        this.getSingle = this.getSingle.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
    }

    // Метод для створення нового запису у базі даних для вказаної таблиці.
    async create(req: Request, res: Response) {
        try {
            // Створення нового запису у базі даних
            // req.body - дані запису для створення
            const data = await this.databaseservice.create(this.table, req.body);

            // Успішна відповідь з кодом статусу 201 (створено) та об'єктом з повідомленням та створеним записом.
            res.status(201);
            res.json({ message: `${this.table} created`, data });
        } catch (error) {
            // Обробка помилки при створенні запису у базі даних.
            res.status(500);
            res.json({ message: `Error creating ${this.table}` });
        }
    }

    // Метод для отримання списку записів з бази даних для вказаної таблиці.
    async getList(req: Request, res: Response) {
        try {
             // Визначення параметрів запиту за замовчуванням
             // limit - кількість записів, які повинні бути отримані
             // skip - кількість записів, які необхідно пропустити
            
            const limit = Number(req.query.limit) || 2
            const skip = Number(req.query.skip) || 0

            // Отримання списку записів з бази даних
            const data = await this.databaseservice.getList(this.table, {
                limit,
                skip,
            });

            // Відповідь з кодом статусу 200 та об'єктом з повідомленням та списком записів
            res.status(200);
            res.json({ data });
        } catch (error) {
            // Обробка помилки при отриманні списку записів
            res.status(500);
            res.json({ message: `${this.table} not exists` });
        }
    }

    // Метод для отримання одного запису з бази даних
    async getSingle(req: Request, res: Response) {
        try {
            // Отримання одного запису з бази даних
            // req.params.id - ID запису
            const data = await this.databaseservice.getSingle(this.table, Number(req.params.id));            
            if (!data) {
                return res.status(404).json({ message: 'Post not found' });
            }
            res.status(200);
            res.json({ data, message: `${this.table} read` });
        } catch (error) {
            res.status(500);
            res.json({ message: 'Internal server error' });
        }
    }

    // Метод для оновлення одного запису
    async update(req: Request, res: Response) {
        try {
            // Оновлення одного запису
            // req.params.id - ID запису
            // req.body - нові дані
            const data = await this.databaseservice.update(this.table, Number(req.params.id), req.body, res);
            
            res.status(200);
            res.json({ data, message: `${this.table} updated` });
        } catch (error) {
            res.status(500);
            throw new Error(`Error updating ${this.table}`);
        }
    }

    // Метод для видалення одного запису
    async delete(req: Request, res: Response) {
        try {
            // Видалення одного запису
            // req.params.id - ID запису
            const data = await this.databaseservice.delete(this.table, Number(req.params.id), res);
            
            res.status(200);
            res.json({ data, message: `${this.table} was removed` });
        } catch (error) {
            res.status(500);
            throw new Error(`Error removing ${this.table}`);
        }
    }
}