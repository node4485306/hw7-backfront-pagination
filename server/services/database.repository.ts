import knex from 'knex';
import path from 'path';
import DatabaseSchemas from '../schemas'

// клас DatabaseRepository це DAL (Data Access Layer) для роботи з базою даних
class DatabaseRepository {
  private static instance: DatabaseRepository;
  private db: any;

  constructor() {
    // ініціалізація бази даних
    this.db = knex({
      client: 'sqlite3',
      connection: {
        // використовуємо SQLite3 для бази даних
        filename: path.join(__dirname, '../db.sqlite')
      },
      useNullAsDefault: true
    });
  }

  // ініціалізація екземпляра класу(патерн Singleton)
  public static getInstance(): DatabaseRepository {
    if (!DatabaseRepository.instance) {
      DatabaseRepository.instance = new DatabaseRepository();
    }
    return DatabaseRepository.instance;
  }

  // створення таблиць у базі даних на основі схеми
  async createTables() {
    for (const schema of DatabaseSchemas) {
      // перевірка чи таблиця існує
      const tableExists = await this.db.schema.hasTable(schema.name);

      // якщо таблиця не існує
      if (!tableExists) {
        // створення таблиці
        await this.db.schema.createTable(schema.name, (table: any) => {
          for (const field of schema.fields) {
            // створення полів з вказаними типами
            table[field.type as string](field.name);
          }
        });
      }
    }
  }

  // видалення таблиць
  async dropTables() {
    for (const schema of DatabaseSchemas) {
      const tableExists = await this.db.schema.hasTable(schema.name);

      if (tableExists) {
        await this.db.schema.dropTable(schema.name);
      }
    }
  }

  // метод для створення нового запису у базі даних для вказаної таблиці
  async create(table: string, data: any) {
    return this.db
      .insert(data)
      .into(table)
      .returning('*')
      .then((rows: any) => rows[0]);
  }

  // метод для отримання списку записів з бази даних для вказаної таблиці
  async readAll(table: string, params: {
    skip: number,
    limit: number
  }) {
    const items = await this.db.select()
      .from(table)
      .limit(params.limit)
      .offset(params.skip)

      const count = await this.db.select().from(table).count().then(
        (rows: any) => rows[0]["count(*)"]
      )

    return {
      items,
      count
    }
  }

  // метод для отримання одного запису з бази даних
  async read(table: string, id: number) {
    return this.db
      .select()
      .from(table)
      .where('id', id)
      .returning('*')
      .then((rows: any) => rows[0]);
  }

  // метод для оновлення одного запису
  async update(table: string, id: number, newData: any) {
    return this.db(table).where('id', id).update(newData);
  }

  // метод для видалення одного запису
  async delete(table: string, id: number) {
    return this.db(table).where('id', id).del();
  }
}

// експортування екземпляра класу
export default DatabaseRepository.getInstance();
