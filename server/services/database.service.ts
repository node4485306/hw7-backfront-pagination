import { Request, Response } from 'express';
import { DatabaseRepository } from '.';

// Клас BaseController є базовим класом для всіх контролерів.
export class DatabaseService {
    

    // Метод для створення нового запису у базі даних для вказаної таблиці.
    async create(table: string, body: any) {

        const data = await DatabaseRepository.create(table, body);
        
        return data;               
    }

    // Метод для отримання списку записів з бази даних для вказаної таблиці.
    async getList(table: string, params: {
        skip: number,
        limit: number
    }) {

        const limit = Number(params.limit) || 2
        const skip = Number(params.skip) || 0

        // Отримання списку записів з бази даних
        const data = await DatabaseRepository.readAll(table, {
            limit,
            skip,
        });

        return data;       
    }

    // Метод для отримання одного запису з бази даних
    async getSingle(table: string, id: number) {

        const data = await DatabaseRepository.read(table, id);            
            
        return data;       
    }

    // Метод для оновлення одного запису
    async update(table: string, id: number, body: any, res: Response) {
        const record = await DatabaseRepository.read(table, id);

        if (!record) {
            return res.status(404).json({ message: 'Post not found' });
        }

        const data = await DatabaseRepository.update(table, id, body);
        return data;
    }

    // Метод для видалення одного запису
    async delete(table: string, id: number, res: Response) {
        const record = await DatabaseRepository.read(table, id);

        if (!record) {
            return res.status(404).json({ message: 'Post not found' });
        }

        const data = await DatabaseRepository.delete(table, id);
        return data;
    }
}

