import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';


function EditNews() {

    const { id } = useParams();
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');

    useEffect(() => {
        // Виконати запит до сервера для отримання конкретної новини за її id
        const fetchNewsPost = async () => {
            try {
                const response = await fetch(`http://localhost:8000/api/post/${id}`);
                const { data } = await response.json();
                setTitle(data.title);
                setContent(data.content);
            } catch (error) {
                console.error('Error fetching news post:', error);
            }
        };
        fetchNewsPost();
    }, [id]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch(`http://localhost:8000/api/post/${id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ title, content })
            });
            if (response.ok) {
                console.log('News post updated successfully!');
            } else {
                console.error('Failed to update news post');
            }
        } catch (error) {
            console.error('Error updating news post:', error);
        }
    };

    return ( 
        <div className='container'>
            <h1>Редагувати новину</h1>

            <form onSubmit={handleSubmit}>

                <div className='input-wrapper'>
                    <label>Title:</label>
                    <textarea value={title} onChange={(e) => setTitle(e.target.value)} />
                </div>
                <div className='input-wrapper'>
                    <label>Content:</label>
                    <textarea value={content} onChange={(e) => setContent(e.target.value)} />
                </div>

                <div className="button-wrapper-between">
                    <Link to={'/'} className='btn'><span>&larr;</span></Link>
                    <button type="submit" className='btn'>Редагувати новину</button>
                </div>                

            </form>
        </div>
    );
}

export default EditNews;
