import React, { useState } from 'react';
import { Link } from 'react-router-dom';


function CreateNews() {

    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch('http://localhost:8000/api/post', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ title, content })
            });
            if (response.ok) {
                console.log('News post created successfully!');
            } else {
                console.error('Failed to create news post');
            }
        } catch (error) {
            console.error('Error creating news post:', error);
        }
    };

    return ( 
        <div className='container'>
            <h1>Створити новину</h1>

            <form onSubmit={handleSubmit}>

                <div className='input-wrapper'>
                    <label>Title:</label>
                    <textarea value={title} onChange={(e) => setTitle(e.target.value)} />
                </div>
                <div className='input-wrapper'>
                    <label>Content:</label>
                    <textarea value={content} onChange={(e) => setContent(e.target.value)} />
                </div>

                <div className="button-wrapper-between">
                    <Link to={'/'} className='btn'><span>&larr;</span></Link>
                    <button type="submit" className='btn'>Створити новину</button>
                </div>                

            </form>
        </div>
    );
}

export default CreateNews;
